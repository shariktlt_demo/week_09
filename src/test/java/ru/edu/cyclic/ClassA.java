package ru.edu.cyclic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Component
@Scope(SCOPE_PROTOTYPE)
@Qualifier("ru.edu.cyclic.ClassA")
public class ClassA implements Element{

    private String name;



    public ClassA(String name) {
        this.name = name;
    }

    //@Autowired
    //@Qualifier("a-class")
    public void setClassA(ClassA classA){

    }

    public ClassA() {
    }

    public void someNameOfInitMethod(){
        name = "Inited";

    }

    public String getName() {
        return name;
    }
}

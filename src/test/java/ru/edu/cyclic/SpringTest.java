package ru.edu.cyclic;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SpringTest {

    @Test
    public void cyclicTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("cyclic.xml");
        Registry bean = context.getBean(Registry.class);
        assertEquals(2, bean.getElements().size());
    }


    @Test
    public void initTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("initDemo.xml");
        ClassA bean = context.getBean("a", ClassA.class);
        ClassA bean2 = context.getBean("a", ClassA.class);
        assertEquals("Inited", bean.getName());

        assertEquals(bean, bean2);

        bean = context.getBean("a2", ClassA.class);
        assertEquals("paramStringAsBean", bean.getName());
    }

    @Test
    public void profileTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("profileDemo.xml");
        ConfigurableEnvironment env = context.getEnvironment();
        //env.setDefaultProfiles("common");
        env.setActiveProfiles("common","PROM");
        context.refresh();

        Service bean = context.getBean(Service.class);
        assertEquals("prom", bean.get());

        env.setActiveProfiles("common","DEV");
        context.refresh();

        bean = context.getBean(Service.class);
        assertEquals("dev", bean.get());

    }

    @Test
    public void prostProcessingTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("postProcessing.xml");
        Service prom = context.getBean("prom", Service.class);

        assertEquals("wrappedValueprom", prom.get());

        Map<String, Element> elementsBeans = context.getBeansOfType(Element.class);

        assertEquals(0, elementsBeans.size());
    }
}
